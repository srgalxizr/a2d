import { Component, OnInit, OnChanges, DoCheck } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  private _languages: string[] = ["", "en", "ru", "he"]; // , "es", "fr", "de", "it", "ru"
  title = "A2D";

  constructor(private readonly _translate: TranslateService) {
    _translate.addLangs(this._languages.sort());
    _translate.setDefaultLang("en");
    _translate.use(
      this._languages.includes(_translate.getBrowserLang())
        ? _translate.getBrowserLang()
        : "en"
    );
    if (_translate.getBrowserLang() === "he") {
      document.dir = "rtl";
    }

    console.log("test");
  } // ctor
} //class
