import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnyServiceComponent } from './any-service.component';

describe('AnyServiceComponent', () => {
  let component: AnyServiceComponent;
  let fixture: ComponentFixture<AnyServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnyServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnyServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
