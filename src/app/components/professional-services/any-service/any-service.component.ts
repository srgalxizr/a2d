import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-any-service",
  templateUrl: "./any-service.component.html",
  styleUrls: ["./any-service.component.scss"],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AnyServiceComponent implements OnInit {
  _displayTxt: string = "";

  constructor(private readonly _ar: ActivatedRoute) {}

  ngOnInit() {
    this._ar.data.subscribe(data => {
      switch (data.service) {
        case "web":
          this._displayTxt = "Web Development";
          break;
        case "mobile":
          this._displayTxt = "Mobile Development";
          break;

        case "consulting":
          this._displayTxt =
            "Software Consulting for Start-ups and Enterprises";
          break;

        case "enterprise":
          this._displayTxt = "Enterprise and Private Training";
          break;

        case "maintenance":
          this._displayTxt = "Software Maintenance";
          break;

        case "testing":
          this._displayTxt = "Software Testing";
          break;

        case "qa":
          this._displayTxt =
            "Quality Assurance - Manual Testing and Automation Development";
          break;

        case "marketing":
          this._displayTxt = "Digital Marketing - SEO, PPC etc";
          break;

        case "analytics":
          this._displayTxt = "Analytics";
          break;

        default:
          this._displayTxt = "";
      } //switch
    });
  } //ng on init
} //class
