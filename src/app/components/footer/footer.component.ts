import { TranslateService } from "@ngx-translate/core";
import { Component, OnInit, ViewEncapsulation } from "@angular/core";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
})
export class FooterComponent implements OnInit {
  _year: number;
  constructor(readonly translate: TranslateService) {}

  ngOnInit() {
    this._year = new Date().getFullYear();
  }
}
