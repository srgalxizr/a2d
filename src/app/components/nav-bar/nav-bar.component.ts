import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: "app-nav-bar",
  templateUrl: "./nav-bar.component.html",
  styleUrls: ["./nav-bar.component.scss"]
})
export class NavBarComponent implements OnInit {
  isShow = false;

  textDir: string = "rtl";
  currentLang: string = "he";

  constructor(public readonly _translate: TranslateService) {}
  ngOnInit() {}

  handleOnLanguageChange(selected: string) {
    this.isShow = !this.isShow;

    if (selected !== "") {
      this._translate.use(selected);

      this.currentLang = selected;
      this.textDir = this.currentLang === "he" ? "rtl" : "ltr";
      document.dir = this.textDir;
    }
  }
} //class
