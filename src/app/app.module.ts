import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { AppRoutingModule, _routingcomponents } from "./app-routing.module";
import { PrimeNGModule } from "./modules/prime-ng/prime-ng.module";
import { MaterialModule } from "./modules/material/material.module";
// import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { NavBarComponent } from "./components/nav-bar/nav-bar.component";
import { FooterComponent } from "./components/footer/footer.component";
import { AnyServiceComponent } from "./components/professional-services/any-service/any-service.component";
import { FlexLayoutModule } from "@angular/flex-layout";
import { BidiModule } from "@angular/cdk/bidi";

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, "/assets/locale/i18n/", ".json");
}

@NgModule({
  declarations: [
    AppComponent,
    _routingcomponents,
    NavBarComponent,
    FooterComponent,
    AnyServiceComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    PrimeNGModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    BidiModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        deps: [HttpClient],
        useFactory: HttpLoaderFactory
      }
    })

    // NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
