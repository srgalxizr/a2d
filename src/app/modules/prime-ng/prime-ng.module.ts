import { NgModule } from "@angular/core";

import { SelectButtonModule } from "primeng/selectbutton";

const primeNgComponentsModules = [SelectButtonModule];

@NgModule({
  imports: [primeNgComponentsModules],
  exports: [primeNgComponentsModules]
})
export class PrimeNGModule {}
