import { NgModule } from "@angular/core";
import {
  MatButtonModule,
  MatSelectModule,
  MatLabel,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatSidenavModule,
  MatListModule
} from "@angular/material";

const materialComponentsModules = [
  MatButtonModule,
  MatSelectModule,
  MatToolbarModule,
  MatIconModule,
  MatMenuModule,
  MatToolbarModule,
  MatSidenavModule,
  MatListModule
];

@NgModule({
  exports: [materialComponentsModules],
  imports: [materialComponentsModules]
})
export class MaterialModule {}
