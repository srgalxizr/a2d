import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HomeComponent } from "./components/home/home.component";
import { AboutComponent } from "./components/about/about.component";
import { ProfessionalServicesComponent } from "./components/professional-services/professional-services.component";
import { ContactComponent } from "./components/contact/contact.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { AnyServiceComponent } from "./components/professional-services/any-service/any-service.component";

export const _routingcomponents = [
  HomeComponent,
  AboutComponent,
  ProfessionalServicesComponent,
  ContactComponent,
  PageNotFoundComponent
];

const routes: Routes = [
  { path: "home", component: HomeComponent },
  { path: "about", component: AboutComponent },
  {
    path: "services",
    component: ProfessionalServicesComponent,
    children: [
      { path: "web", component: AnyServiceComponent, data: { service: "web" } },
      {
        path: "consulting",
        component: AnyServiceComponent,
        data: { service: "consulting" }
      },
      {
        path: "enterprise",
        component: AnyServiceComponent,
        data: { service: "enterprise" }
      },
      {
        path: "maintenance",
        component: AnyServiceComponent,
        data: { service: "maintenance" }
      },
      {
        path: "testing",
        component: AnyServiceComponent,
        data: { service: "testing" }
      },
      {
        path: "mobile",
        component: AnyServiceComponent,
        data: { service: "mobile" }
      },
      {
        path: "qa",
        component: AnyServiceComponent,
        data: { service: "qa" }
      },
      {
        path: "marketing",
        component: AnyServiceComponent,
        data: { service: "marketing" }
      },
      {
        path: "analytics",
        component: AnyServiceComponent,
        data: { service: "analytics" }
      }
    ]
  },
  { path: "contact", component: ContactComponent },
  { path: "", pathMatch: "full", redirectTo: "/home" },
  { path: "**", component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
